async function LoadHome(pagenum)
{
    $("#loading").append
    (`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    const response=await fetch(`https://api.themoviedb.org/3/movie/popular?api_key=d44fbeec92b0e369fb74d2647d068707&page=${pagenum}`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    Print(user,1,pagenum);
}
async function Search(pagenum)
{
    $("#loading").append(`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    query= $("#searchname").val();
    response=await fetch(`https://api.themoviedb.org/3/search/movie?api_key=d44fbeec92b0e369fb74d2647d068707&query=${query}&page=${pagenum}`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    Print(user,2,pagenum);
}
//Hàm in ra danh sách phim
function Print(user,option,pagenum)
{
    if(option==1) t='LoadHome';
    else if(option==2) t='Search'
    $(".maincontent").remove();
    $("#content").append(`<div class="maincontent"></div>`)
    for(let i=0;i<20;i++)
    {
        if(i<user.results.length)
        {
            $(".maincontent").append(
                `
                <div class="shadow-sm p-3 mb-5" id="card" style= "background-color: rgba(255,255,255,0.5);">
                    <div class="card h-100" style="width: 15rem;" href=# onclick="Detail(${user.results[i].id})">
                        <img src="https://image.tmdb.org/t/p/w500${user.results[i].poster_path}" class="card-img-top" alt="Poster">
                            <div class="center" style="color: red;"><b>${user.results[i].title}</b></div>
                            <div class="center"><b>Ngày công chiếu: ${user.results[i].release_date}</b></div>
                            <div class="center"><b>Độ phổ biến: ${user.results[i].popularity}</b></div>
                            <div class="center"><b>Điểm số: ${user.results[i].vote_average}</b></div>
                    </div>
                </div>`
            )
        }
    }
    $(".pagination").remove();
    $("#pagination-nav").append(
        `<ul class="pagination">
                <li class="page-item">
                <a class="page-link" href="#"onclick="${t}(${pagenum-1})">Previous</a>
            </li>
            <li class="page-item active" id="pg">
                <a class="page-link" href="#" onclick= "${t}(${pagenum})">${pagenum}</a>
            </li>
            <li class="page-item" id="pg2">
                <a class="page-link" href="#" onclick= "${t}(${pagenum+1})">${pagenum+1}</a>
            </li>
            <li class="page-item" id="pg3">
                <a class="page-link" href="#" onclick="${t}(${pagenum+2})">${pagenum+2}</a>
            </li>
            <li class="page-item">
                    <a class="page-link">...</a>
                </li>
            <li class="page-item">
                <a class="page-link" href="#"onclick="${t}(${pagenum+1})">Next</a>
            </li>
        </ul>
    `);
}
async function Search2(pagenum)
{
    $("#loading").append(`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    query= $("#searchactor").val();
    response=await fetch(`https://api.themoviedb.org/3/search/person?api_key=d44fbeec92b0e369fb74d2647d068707&query=${query}&page=${pagenum}`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    Print2(user,pagenum);
}
//Hàm in ra danh sách phim
function Print2(user,pagenum)
{
    $(".maincontent").remove();
    $("#content").append(`<div class="maincontent"></div>`)
    for(let i=0;i<20;i++)
    {
        if(i<user.results.length)
        {
            $(".maincontent").append(
                `
                <div class="shadow-sm p-3 mb-5" id="card" style= "background-color: rgba(255,255,255,0.5);">
                    <div class="card h-100" style="width: 15rem;" onclick="DetailActor(${user.results[i].id})">
                        <img src="https://image.tmdb.org/t/p/w500${user.results[i].profile_path}" class="card-img-top" alt="No Picture">
                            <div class="center" style="color: red;"><b>${user.results[i].name}</b></div>
                            <div class="center"><b>Thể loại chính: ${user.results[i].known_for_department}</b></div>
                            <button type="button" class="btn btn-outline-success" onclick="ListMoviesOfActor(${user.results[i].id},1)">Danh sách phim</button>
                    </div>
                </div>`
            )
        }
    }
    $(".pagination").remove();
    $("#pagination-nav").append(
        `<ul class="pagination">
                <li class="page-item">
                <a class="page-link" href="#"onclick="Search2(${pagenum-1})">Previous</a>
            </li>
            <li class="page-item active" id="pg">
                <a class="page-link" href="#" onclick= "Search2(${pagenum})">${pagenum}</a>
            </li>
            <li class="page-item" id="pg2">
                <a class="page-link" href="#" onclick= "Search2(${pagenum+1})">${pagenum+1}</a>
            </li>
            <li class="page-item" id="pg3">
                <a class="page-link" href="#" onclick="Search2(${pagenum+2})">${pagenum+2}</a>
            </li>
            <li class="page-item">
                    <a class="page-link">...</a>
                </li>
            <li class="page-item">
                <a class="page-link" href="#"onclick="Search2(${pagenum+1})">Next</a>
            </li>
        </ul>
    `);
}
async function ListMoviesOfActor(ActorID,pagenum)
{
    $("#loading").append(`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    response=await fetch(`https://api.themoviedb.org/3/person/${ActorID}/movie_credits?api_key=d44fbeec92b0e369fb74d2647d068707`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    Print3(ActorID,user,pagenum);
}
function Print3(ActorID,user,pagenum)
{
    $(".maincontent").remove();
    $("#content").append(`<div class="maincontent"></div>`)
    for(let i=20*(pagenum-1);i<20*pagenum;i++)
    {
        if(i<user.cast.length)
        {
            $(".maincontent").append(
                `
                <div class="shadow-sm p-3 mb-5" id="card" style= "background-color: rgba(255,255,255,0.5);">
                    <div class="card h-100" style="width: 15rem;" href=# onclick="Detail(${user.cast[i].id})">
                        <img src="https://image.tmdb.org/t/p/w500${user.cast[i].poster_path}" class="card-img-top" alt="Poster">
                            <div class="center" style="color: red;"><b>${user.cast[i].title}</b></div>
                            <div class="center"><b>Ngày công chiếu: ${user.cast[i].release_date}</b></div>
                            <div class="center"><b>Độ phổ biến: ${user.cast[i].popularity}</b></div>
                            <div class="center"><b>Điểm số: ${user.cast[i].vote_average}</b></div>
                    </div>
                </div>`
            )
        }
    }
    $(".pagination").remove();
    $("#pagination-nav").append(
        `<ul class="pagination">
                <li class="page-item">
                <a class="page-link" href="#"onclick="ListMoviesOfActor(${ActorID},${pagenum-1})">Previous</a>
            </li>
            <li class="page-item active" id="pg">
                <a class="page-link" href="#" onclick= "ListMoviesOfActor(${ActorID},${pagenum})">${pagenum}</a>
            </li>
            <li class="page-item" id="pg2">
                <a class="page-link" href="#" onclick= "ListMoviesOfActor(${ActorID},${pagenum+1})">${pagenum+1}</a>
            </li>
            <li class="page-item" id="pg3">
                <a class="page-link" href="#" onclick="ListMoviesOfActor(${ActorID},${pagenum+2})">${pagenum+2}</a>
            </li>
            <li class="page-item">
                    <a class="page-link">...</a>
                </li>
            <li class="page-item">
                <a class="page-link" href="#"onclick="ListMoviesOfActor(${ActorID},${pagenum+1})">Next</a>
            </li>
        </ul>
    `);
}
async function Detail(MovieID)
{
    $("#loading").append
    (`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    const response=await fetch(`https://api.themoviedb.org/3/movie/${MovieID}?api_key=d44fbeec92b0e369fb74d2647d068707`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    $(".maincontent").remove();
    $(".pagination").remove();
    $("#content").append(`
        <div class="maincontent">
            <div class="card"style= "background-color: rgba(255,255,255,0.7);">
                <div class="row"style="padding-left:0px;">
                    <div class="col-md-6">
                        <img src="https://image.tmdb.org/t/p/original/${user.poster_path}" class="card-img" alt="Poster">
                    </div>
                    <div class="col-md-6" style="padding-left:0px;">
                        <div class="card-body" style="padding-left:0px;padding-right:45px;">
                            <h1 class="tittle" style="padding-left:30px;">${user.title}</h1>
                            <ul>
                                <li class="list-group-item" id="genres">Thể loại: </li>
                                <li class="list-group-item" id="companies">Sản xuất: </li>
                                <li class="list-group-item" id="actors">Diễn viên: </li>
                                <li class="list-group-item">Ngày công chiếu: ${user.release_date}</li>
                                <li class="list-group-item">Thời gian: ${user.runtime}:00</li>
                                <li class="list-group-item">Điểm số: ${user.vote_average}</li>
                                <li class="list-group-item">Tóm tắt: ${user.overview}</li>
                                <center>
                                <button type="button" class="btn btn-success" style="width: 10rem;" onclick="ReviewsMovie(${MovieID})">Reviews</button>
                                <button type="button" class="btn btn-danger" style="width: 10rem;" onclick="PlayTrailer(${MovieID})">Trailer</button>    
                                </center>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
    for(i=0;i<user.genres.length-1;i++)
    {
        $("#genres").append(`${user.genres[i].name}, `);
    }
    $("#genres").append(`${user.genres[user.genres.length-1].name}`);
    for(i=0;i<user.production_companies.length-1;i++)
    {
        $("#companies").append(`${user.production_companies[i].name}, `);
    }
    $("#companies").append(`${user.production_companies[user.production_companies.length-1].name}`);
}
async function ReviewsMovie(MovieID)
{
    $("#loading").append
    (`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    const response=await fetch(`https://api.themoviedb.org/3/movie/${MovieID}/reviews?api_key=d44fbeec92b0e369fb74d2647d068707`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    $(".maincontent").remove();
    $("#content").append(`
        <div class="maincontent">
            <div class="card">
                <div class="card-header">REVIEWS</div>
                <div class="card-body">
                    <blockquote class="blockquote mb-0">
                        <p>${user.results[0].content}</p>
                        <footer class="blockquote-footer">${user.results[0].author}</footer>
                    </blockquote>
                </div>
            </div>
        </div>
    `);
}
async function PlayTrailer(MovieID)
{
    $("#loading").append
    (`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    const response=await fetch(`https://api.themoviedb.org/3/movie/${MovieID}/videos?api_key=d44fbeec92b0e369fb74d2647d068707`);
    const str=await response.text();
    const user=JSON.parse(str);
    $("#spinner").remove();
    $(".maincontent").remove();
    $("#content").append(`
        <div class="maincontent">
        <center>
                <div class="col-md-12" style="background-color:rgba(255,255,255,0.7);padding:20px 10px">
                    <iframe width="1120" height="630"src="https://www.youtube.com/embed/${user.results[0].key}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
                </center>
        </div>
    `);
}
async function DetailActor(ActorID)
{
    $("#loading").append
    (`
        <div class="spinner-border text-light" id="spinner" role="status">
            <span class="sr-only">Loading...</span>
        </div>
    `);
    const response=await fetch(`https://api.themoviedb.org/3/person/${ActorID}?api_key=d44fbeec92b0e369fb74d2647d068707`);
    const str=await response.text();
    const user=JSON.parse(str);
    console.log(user)
    $("#spinner").remove();
    $(".maincontent").remove();
    $(".pagination").remove();
    $("#content").append(`
        <div class="maincontent">
            <div class="card"style= "background-color: rgba(255,255,255,0.7);">
                <div class="row"style="padding-left:0px;">
                    <div class="col-md-6">
                        <img src="https://image.tmdb.org/t/p/w500${user.profile_path}" class="card-img" alt="Poster">
                    </div>
                    <div class="col-md-6" style="padding-left:0px;">
                        <div class="card-body" style="padding-left:0px;padding-right:45px;">
                            <h1 class="tittle" style="padding-left:30px;">${user.name}</h1>
                            <ul>
                                <li class="list-group-item">Ngày sinh: ${user.birthday}</li>
                                <li class="list-group-item">Nơi sinh: ${user.place_of_birth}</li>
                                <li class="list-group-item">Thể loại: ${user.known_for_department}</li>
                                <li class="list-group-item">Tiểu sử: ${user.biography}</li>
                                <li class="list-group-item">
                                <center>
                                <button type="button" class="btn btn-success" onclick="ListMoviesOfActor(${ActorID},1)">Danh sách phim</button>
                                </center>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>`);
}